package com.messanger.resources;


import com.entity.Message;
import com.messanger.services.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/messenger")
public class MessengerResource {


    MessageService messageService = new MessageService();

    @GET()
    @Path("/all")
    @Produces(MediaType.APPLICATION_XML)
    public List<Message> getMessenger(){
        return messageService.getAllMessages();

    }

    @GET
    @Path("/message/{id: \\d+}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("id") String id){
        return Response.status(200).entity("Digit only allow ,d :"+ id).build();

    }

    @GET
    @Path("/book/{isbn: \\d+}")
     public Response getUserBookByIsbn(@PathParam("isbn") String isbn){
        return Response.status(200).entity("getUserBookByIsbn ,isbn :"+ isbn).build();

    }

    @GET
    @Path("/username/{username: [a-zA-Z][a-zA-Z_0-9]}")
     public Response getUserByUsername(@PathParam("username") String username){
        return Response.status(200).entity("getUserBookByIsbn ,username :"+ username).build();

    }

    //multi parameter
    @GET
    @Path("/{year}/{month}/{day}")
    public Response getUserHistory(@PathParam("year") int year,@PathParam("month") int month, @PathParam("day") int day)
    {
        String date = year +"/" + month +"/" +day;
        return Response.status(200).entity("getUserHistory" +date).build();
    }

    //passing list as query parameters
    @GET
    @Path("/query")
    public Response getListOfUsers(@QueryParam("from") String from, @QueryParam("to") String to,
                                   @QueryParam("orderBy") List<String> orderBy){
        return Response.status(200).entity("getUser called :" + from  +" to :" + to + " OrderBy :" + orderBy.toString()).build();

    }

    @POST
    @Path("/add")
    public Response addUser(@FormParam("name") String name , @FormParam("age") String age, @FormParam("country") String country){
        return Response.status(200).entity("addUser is callled , name :" + name + " Age :"+age + " :" + country).build();
    }


}
