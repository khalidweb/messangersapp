package com.messanger.file.resources;

import jdk.nashorn.internal.objects.annotations.Getter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;

@Path("/file")
public class FileService {

    private static final String FILE_PATH = "C:\\Users\\abbasi\\Desktop\\global-groovy.log";
    private static final String IMAGE_PATH = "C:\\Users\\abbasi\\Desktop\\Mockup Mathleaks\\mlvideo.png";

    @GET
    @Path("/get")
    @Produces("text/plain")
    public Response getDownloadFile(){
        File file = new File(FILE_PATH);
        Response.ResponseBuilder responseBuilder = Response.ok((Object)file);

        responseBuilder.header("Content-Disposition", "attachment;filename= \"file_from_server.log\"");
        return responseBuilder.build();
    }

    @GET
    @Path("/image")
    @Produces("image/png")
    public Response getDownloadImage(){
        File image = new File(IMAGE_PATH);
        Response.ResponseBuilder responseBuilder = Response.ok((Object)image);
        responseBuilder.header("Content-Disposition", "attachment; filename= \"file_from_desktop.png\"");
        return responseBuilder.build();

    }

}
