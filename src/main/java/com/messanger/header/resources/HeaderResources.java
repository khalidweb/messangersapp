package com.messanger.header.resources;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Path("/header")
public class HeaderResources {

    //Headerparam way to get user agent
    @GET
    @Path("/useragent")
    public Response getHeaderUsers(@HeaderParam("user-agent") String userAgent){
        return Response.status(200)
                .entity("getHeaderUser called, :User-Agent " +userAgent).build();
    }

    //Context way to get headers
    @GET
    @Path("/context")
    public Response getUserHeadersWithContext(@Context HttpHeaders httpHeaders){
        String userAgent = httpHeaders.getRequestHeader("user-agent").get(0);
        return Response.status(200)
                .entity("Context way of headers , userAgent " + userAgent).build();
    }

    //List all available headers

    @GET
    @Path("/listheaders")
    public Response getListOfHeaders(@Context HttpHeaders httpHeaders) {
        for (String headers : httpHeaders.getRequestHeaders().keySet()) {
            System.out.println(headers);
        }
        return null;
    }
}
