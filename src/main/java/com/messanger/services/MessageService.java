package com.messanger.services;

import com.entity.Message;
import org.jvnet.hk2.annotations.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    public List<Message> getAllMessages(){
        List<Message> list = new ArrayList<>();
        list.add(new Message(1L,"First Message","khalid", LocalDate.now()));
        list.add(new Message(2L,"secod Message","abbasi", LocalDate.now()));
        return list;
    }

    public void saveMessage(Message message){

    }
}
