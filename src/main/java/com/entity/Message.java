package com.entity;



import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;


@XmlRootElement
public class Message {
    private Long id;
    private String message;
    private String author;
    private LocalDate created;

    public Message(){}

    public Message(Long id, String message, String author, LocalDate created) {
        this.id = id;
        this.message = message;
        this.author = author;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }



}
